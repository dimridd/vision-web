from rest_framework import serializers

from cb_vision.apis.models import UploadImage, APIdoc, Payment


class UploadImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = UploadImage
        fields = "__all__"

    def update(self, instance, validated_data):
        instance.title = validated_data.get("title", instance.title)
        instance.image_file = validated_data.get("image_file", instance.image_file)
        if instance.title is not None and instance.image_file is not None:
            instance.save()

        return instance


class APIDocSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIdoc
        fields = "__all__"


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = "__all__"
