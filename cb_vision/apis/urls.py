from django.conf.urls import url
from .views import DetectFace, DeleteImagesDetail, UploadImageDetail, ReconstructionFace, BeautyScore, \
    DetectionBody, OutlineBody, DenseLandmark, EmotionFace, LandmarkFace, ComparingFace, SerchingFace, \
    SkinStatus, GazeEstimation, GestureHand, SkeletonDetection, APIDoc, GetAPIKey, Balance, PaymentView, HelloView, \
    FacePose, FaceSmile, Age, EyeStatus, Gender, EyeGlasses, BodyAttribute, FaceEmbedding, AddEmbedding
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

app_name = "apis"

urlpatterns = [

    ######################## Face Operations APIs ########################
    # Face Operations
    url(r"^(?P<user>\w+)/face_detection/$", DetectFace.as_view(), name='face_detection'),
    url(r"^(?P<user>\w+)/gaze_estimation/$", GazeEstimation.as_view(), name='gaze_estimation'),
    url(r"^(?P<user>\w+)/facial_skin_status_analyze/$", SkinStatus.as_view(), name='facial_skin_status_analyze'),
    url(r"^(?P<user>\w+)/face_searching/$", SerchingFace.as_view(), name='face_searching'),
    url(r"^(?P<user>\w+)/face_landmarks/$", LandmarkFace.as_view(), name='face_landmarks'),
    url(r"^(?P<user>\w+)/face_comparing/$", ComparingFace.as_view(), name='face_comparing'),
    url(r"^(?P<user>\w+)/emotion_recognition/$", EmotionFace.as_view(), name='emotion_recognition'),
    url(r"^(?P<user>\w+)/dense_facial_landmarks/$", DenseLandmark.as_view(), name='dense_facial_landmarks'),
    url(r"^(?P<user>\w+)/beauty_score/$", BeautyScore.as_view(), name='beauty_score'),
    url(r"^(?P<user>\w+)/3d_face_model_reconstruction/$", ReconstructionFace.as_view(),
        name='3d_face_model_reconstruction'),

    # Human Body Operations
    url(r"^(?P<user>\w+)/skeleton_detection/$", SkeletonDetection.as_view(), name='skeleton_detection'),
    url(r"^(?P<user>\w+)/body_detect/$", DetectionBody.as_view(), name='body_detect'),
    url(r"^(?P<user>\w+)/body_outline/$", OutlineBody.as_view(), name='body_outline'),
    url(r"^(?P<user>\w+)/hand_gesture/$", GestureHand.as_view(), name='hand_gesture'),
    url(r"^(?P<user>\w+)/body_attribute/$", BodyAttribute.as_view(), name='body_attribute'),

    # Face Attributes Operations
    url(r"^(?P<user>\w+)/face_pose/$", FacePose.as_view(), name='body_attribute'),
    url(r"^(?P<user>\w+)/face_smile/$", FaceSmile.as_view(), name='body_attribute'),
    url(r"^(?P<user>\w+)/age/$", Age.as_view(), name='body_attribute'),
    url(r"^(?P<user>\w+)/eye_status/$", EyeStatus.as_view(), name='body_attribute'),
    url(r"^(?P<user>\w+)/gender/$", Gender.as_view(), name='body_attribute'),
    url(r"^(?P<user>\w+)/glasses/$", EyeGlasses.as_view(), name='body_attribute'),

    # Adding and Creating Face Embedding for Comparing/Searching
    url(r"^(?P<user>\w+)/create_face_embedding/$", FaceEmbedding.as_view(), name='create_face_embedding'),
    url(r"^(?P<user>\w+)/add_face_embedding/$", AddEmbedding.as_view(), name='add_face_embedding'),

    ############################ unused APIs #############################
    url(r"^pay/$", PaymentView.as_view(), name='pay'),
    url(r"^balance/(?P<user>\w+)/$", Balance.as_view(), name='user_balance'),
    url(r"^get_api_key/(?P<user>\w+)/$", GetAPIKey.as_view(), name='get_api_key'),
    path('upload/', UploadImageDetail.as_view(), name='file_upload'),
    url(r"^api_doc/(?P<title>\w+)/$", APIDoc.as_view(), name='api_doc'),
    path('delete/', DeleteImagesDetail.as_view(), name='delete-image'),
    path('hello/', HelloView.as_view(), name='hello'),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),  # <-- And here
]
