from django.apps import AppConfig


class ApisConfig(AppConfig):
    name = 'cb_vision.apis'
