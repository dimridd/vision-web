# Django imports
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings

# Third party imports
from model_utils.models import TimeStampedModel


# Create your models here.

CHOICES = (
    ("None", "None"),
    ("3d_face_model_reconstruction", "3d_face_model_reconstruction"),
    ("beauty_score", "beauty_score"),
    ("detected_color", "detected_color"),
    ("detected_body", "detected_body"),
    ("outlined_image", "outlined_image"),
    ("landmark_image", "landmark_image"),
    ("emotion", "emotion"),
    ("face_atttributes", "face_atttributes"),
    ("compare_result", "compare_result"),
    ("detected_face", "detected_face"),
    ("search_result", "search_result"),
    ("skin_status_analyze", "skin_status_analyze"),
    ("gaze_estimation", "gaze_estimation"),
    ("hand_gesture", "hand_gesture"),
    ("skeleton_detection", "skeleton_detection"),
)

PRICE_CHOICES = (
    ("None", "None"),

    ("gender_detection", "gender_detection"),
    ("face_smiling", "face_smiling"),
    ("face_gaze", "face_gaze"),
    ("eye_glass_status", "eye_glass_status"),
    ("eye_status", "eye_status"),
    ("age", "age"),

    ("3d_face_model_reconstruction", "3d_face_model_reconstruction"),
    ("beauty_score", "beauty_score"),
    ("detected_color", "detected_color"),
    ("detected_body", "detected_body"),
    ("outlined_image", "outlined_image"),
    ("landmark_image", "landmark_image"),
    ("emotion", "emotion"),
    ("face_pose", "face_pose"),
    ("compare_result", "compare_result"),
    ("detected_face", "detected_face"),
    ("search_result", "search_result"),
    ("skin_status_analyze", "skin_status_analyze"),
    ("gaze_estimation", "gaze_estimation"),
    ("hand_gesture", "hand_gesture"),
    ("skeleton_detection", "skeleton_detection"),
    ("face_beauty", "face_beauty"),
    ("face_merging", "face_merging"),
    ("merged_images", "merged_images")
)


class Payment(TimeStampedModel):
    name = models.CharField(
        _("Name"), max_length=255, blank=True, null=True
    )


class ServiceCost(TimeStampedModel):

    operation = models.CharField(
        _("Operation"),
        choices=PRICE_CHOICES,
        default="None",
        max_length=100,
    )
    price = models.FloatField(_("Price"), default=0)
    image_link1 = models.ImageField(_("Image I"), null=True, blank=True,)
    image_link2 = models.ImageField(_("Image II"), null=True, blank=True,)
    image_link3 = models.ImageField(_("Image III"), null=True, blank=True,)
    image_link4 = models.ImageField(_("Image IV"), null=True, blank=True,)
    image_link5 = models.ImageField(_("Image V"), null=True, blank=True,)

    def __str__(self):
        return self.operation


class UploadImage(TimeStampedModel):

    title = models.CharField(
        _("Title"), max_length=255, blank=True, null=True
    )
    image_file = models.ImageField(
        _("Image"), null=True, blank=True, upload_to="images/"
    )

    def __str__(self):
        return self.title


class Requestparam(TimeStampedModel):
    title = models.CharField(
        _("Title"), max_length=255, blank=True, null=True
    )
    api_key = models.TextField(
        _("API Key"))
    api_key_type = models.CharField(_("API Key Type"), max_length=255,)

    api_secret = models.TextField(
        _("API Secret")
    )
    api_secret_type = models.CharField(_("API Secret Type"), max_length=255,)

    image_file = models.TextField(
        _("Image File")
    )
    image_file_type = models.CharField(_("Image File Type"), max_length=255,)

    def __str__(self):
        return self.title


class ReturnVal(TimeStampedModel):
    title = models.CharField(
        _("Title"), max_length=255, blank=True, null=True
    )
    request_id = models.TextField(
        _("Request ID"))
    request_id_type = models.CharField(_("Request ID Type"), max_length=255,)

    image_id = models.TextField(
        _("Image ID")
    )
    image_id_type = models.CharField(_("Image ID Type"), max_length=255,)

    error_message = models.TextField(
        _("Error Msg")
    )
    error_message_type = models.CharField(_("Error Msg Type"), max_length=255,)

    operation = models.CharField(
        _("Operation Choices"),
        choices=CHOICES,
        default="None",
        max_length=100,
    )
    operation_type = models.CharField(_("Operation Type"), max_length=255,)
    operation_description = models.TextField(
        _("Description"), default="None"
    )

    def __str__(self):
        return self.title


class APIdoc(TimeStampedModel):
    title = models.CharField(
        _("Title"), max_length=255, blank=True, null=True
    )
    version = models.PositiveSmallIntegerField(_("Version"), default=0.0)
    overview = models.TextField(_("Overview"))
    image_requirement = models.TextField(_("Image Requirement"))
    request_url = models.URLField(_("Request URL"), max_length=200)
    request_method = models.CharField(_("Request Method"), max_length=255)
    permission = models.CharField(_("permission"), max_length=255)

    request_parameters = models.ForeignKey(Requestparam, on_delete=models.CASCADE, null=True)
    return_value = models.ForeignKey(ReturnVal, on_delete=models.CASCADE, null=True)

    sample_response = models.TextField(_("Sample Response"))
    error_messages = models.TextField(_("Error Messages"))
    sample_request = models.TextField(_("Sample Request"))

    def __str__(self):
        return self.title

#
# class APIUserCount(TimeStampedModel):
#     user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
#     api_doc = models.ForeignKey(APIdoc, on_delete=models.CASCADE, null=True,)
#     api_count = models.PositiveSmallIntegerField(_("Api count"), default=0)
#
#     def __str__(self):
#         return self.api_doc.title
