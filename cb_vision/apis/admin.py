from django.contrib import admin

# Register your models here.

from .models import UploadImage, APIdoc, Requestparam, ReturnVal, ServiceCost, Payment

admin.site.register(UploadImage)
admin.site.register(Requestparam)
admin.site.register(APIdoc)
admin.site.register(ReturnVal)
# admin.site.register(APIUserCount)
admin.site.register(ServiceCost)
admin.site.register(Payment)
