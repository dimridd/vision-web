"""
Calling:
    1. <upload> api call
    2. <delete> api call
    3. <service> api call
"""
# Third party imports
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from cb_vision.memberships.models import UserMembership, Subscription
from cb_vision.users.models import User
from cb_vision.apis.serializers import UploadImageSerializer, APIDocSerializer, PaymentSerializer
from cb_vision.apis.models import UploadImage, APIdoc, Payment
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
import os
import json
import razorpay
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import smtplib

# Local imports
import requests
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect

User = get_user_model()

# Create your views here.
SUCCESS = "success"
DELETE_SUCCESS = "deleted"

from django.shortcuts import get_object_or_404
from rest_framework.renderers import TemplateHTMLRenderer


class PaymentView(APIView):
    # # permission_classes = (AllowAny, )
    # serializer_class = PaymentSerializer
    #
    # def post(self, request, *args, **kwargs):
    #
    #     data = request.name
    #     serializer = PaymentSerializer(name=data)
    # renderer_classes = [TemplateHTMLRenderer]
    template_name = 'index.html'

    def get(self, request):
        profile = get_object_or_404(Payment)
        serializer = PaymentSerializer(profile)
        return Response({'serializer': serializer.data})

    def post(self, request):
        profile = get_object_or_404(Payment)
        serializer = PaymentSerializer(name=request.name)
        if not serializer.is_valid():
            return Response({'serializer': serializer, 'profile': profile})
        serializer.save()

        return redirect('pay')

        # new_data = serializer.data
        # name = new_data["name"]
        # name = request.POST.get('name')
        # amount = 50000
        #
        # client = razorpay.Client(
        #     auth=("rzp_test_FiYS35hJuCOj8b", "K4C4WsjKS0gBFcQ0fbLYYPoA"))
        #
        # payment = client.order.create({'amount': amount, 'currency': 'INR',
        #                                'payment_capture': '1'})
        #
        # return render(request, 'index.html')


@csrf_exempt
def success(request):
    return render(request, "success.html")


# class Success(APIView):
#
#     @csrf_exempt
#     def success(self, request):
#         return render(request, "success.html")


class Balance(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, user):
        user = User.objects.filter(username=user)
        cus_count = user.count()
        if not cus_count:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            user_membership = UserMembership.objects.filter(user=user[0])
            subscription = Subscription.objects.filter(user_membership=user_membership[0])
            subscription = subscription[0]

            total_price = subscription.skeleton + subscription.facial_skin_status + \
                          subscription.face_searching + subscription.face_landmarks + \
                          subscription.face_detection + subscription.hand_gesture + \
                          subscription.gaze_estimation + subscription.face_comparing + \
                          subscription.face_attributes_api + subscription.emotion_recognition + \
                          subscription.body_detection + subscription.body_attributes + \
                          subscription.beauty_score + subscription.dense_facial_landmarks + \
                          subscription.body_outline + subscription.face_reconstruction

            used_api = user_membership.count()
            response = {
                "total_api": total_price,
                "used_api": used_api,
                "bonus_account": "bonus_account"
            }
            return Response(response)


class GetAPIKey(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, user):
        user = User.objects.filter(username=user)
        cus_count = user.count()
        if not cus_count:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            user_membership = UserMembership.objects.filter(user=user[0])
            customer_token_id = user_membership[0].customer_token_id

            membership_type = user_membership[0].membership.membership_type

            subscription = Subscription.objects.filter(user_membership=user_membership[0])

            token = Token.objects.get(user=user.first()).key
            sub_active = subscription[0].is_active

            response = {
                "app": "Dummy text",
                "api_key": token,
                "api_secret": customer_token_id,
                "type": membership_type,
                "status": sub_active,
                "operation": "manage"
            }

            return Response(response)


class UploadImageDetail(CreateAPIView, LoginRequiredMixin):
    serializer_class = UploadImageSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        headers = self.get_success_headers(serializer.data)

        data_ = serializer.data
        response = {"output": data_}

        image = os.path.join(
            settings.MEDIA_ROOT, "images", data_["image_file"].split("/")[-1]
        )
        return Response(data=response)


class APIDoc(APIView):
    # permission_classes = (AllowAny, )
    serializer_class = APIDocSerializer

    # permission_classes = (IsAuthenticated, )
    def get(self, request, title):
        # global Doc
        try:
            Doc = APIdoc.objects.filter(title=title)

        except Doc.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = APIDocSerializer(Doc, many=True, context={'request': request})
        if serializer.data[0]["title"] == "Skeleton":
            serializer.data[0]["title"] = "Skeleton Detection API"
        if serializer.data[0]["title"] == "Hand_Gesture":
            serializer.data[0]["title"] = "Hand Gesture API"
        if serializer.data[0]["title"] == "Gaze_Estimation":
            serializer.data[0]["title"] = "Gaze Estimation API"
        if serializer.data[0]["title"] == "Facial_Skin_Status":
            serializer.data[0]["title"] = "Facial Skin Status Analyze API"
        if serializer.data[0]["title"] == "Face_Searching":
            serializer.data[0]["title"] = "Face Searching API"
        if serializer.data[0]["title"] == "Face_Landmarks":
            serializer.data[0]["title"] = "Face Landmarks API"
        if serializer.data[0]["title"] == "Face_Detection":
            serializer.data[0]["title"] = "Face Detection API"
        if serializer.data[0]["title"] == "Face_Comparing":
            serializer.data[0]["title"] = "Face Comparing API"
        if serializer.data[0]["title"] == "Emotion_Recognition":
            serializer.data[0]["title"] = "Emotion Recognition API"
        if serializer.data[0]["title"] == "Dense_Facial_Landmarks":
            serializer.data[0]["title"] = "Dense Facial Landmarks API"
        if serializer.data[0]["title"] == "Body_Outline":
            serializer.data[0]["title"] = "Body Outline API"
        if serializer.data[0]["title"] == "Body_Detection":
            serializer.data[0]["title"] = "Body Detection API"
        if serializer.data[0]["title"] == "Body_Attributes":
            serializer.data[0]["title"] = "Body Attributes API"
        if serializer.data[0]["title"] == "Beauty_Score":
            serializer.data[0]["title"] = "Beauty Score API"
        if serializer.data[0]["title"] == "Face_Reconstruction":
            serializer.data[0]["title"] = "3D Face Model Reconstruction API"

        return Response(data=serializer.data, status=status.HTTP_200_OK)


class FaceEmbedding(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/create_embedding/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class AddEmbedding(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))
        name = str(post[0].title)

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/add_embedding/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path, "name": name})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class DetectFace(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_face/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class SkeletonDetection(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_skeleton/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class GestureHand(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_hand/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class BodyAttribute(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_attribute/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class GazeEstimation(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()
        # url = "http://cbfacedetect.herokuapp.com/detectFace/"
        # url = "https://cbhumandetection.herokuapp.com/human_det/"

        # url = "http://127.0.0.1:8000/human_det/"
        url = "http://127.0.0.1:8000/eye_gaze/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class SkinStatus(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()
        # url = "http://cbfacedetect.herokuapp.com/detectFace/"
        # url = "https://cbhumandetection.herokuapp.com/human_det/"

        # url = "http://127.0.0.1:8000/human_det/"
        url = "http://127.0.0.1:8000/det_skeleton/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class SerchingFace(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/face_search/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class ComparingFace(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/face_compare/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class LandmarkFace(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_landmark/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class AttributeFace(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()
        # url = "http://cbfacedetect.herokuapp.com/detectFace/"
        # url = "https://cbhumandetection.herokuapp.com/human_det/"

        # url = "http://127.0.0.1:8000/human_det/"
        url = "http://127.0.0.1:8000/det_skeleton/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class EmotionFace(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_emotion/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class DenseLandmark(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()
        # url = "http://cbfacedetect.herokuapp.com/detectFace/"
        # url = "https://cbhumandetection.herokuapp.com/human_det/"

        # url = "http://127.0.0.1:8000/human_det/"
        url = "http://127.0.0.1:8000/det_skeleton/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class DetectionBody(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/human_det/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


########################################################################################################
"""
Face Attributes
"""


class FacePose(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()
        # url = "http://cbfacedetect.herokuapp.com/face_pose/"

        print(user_memebership.count)
        url = "http://127.0.0.1:8000/face_pose/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class FaceSmile(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()
        # url = "http://cbfacedetect.herokuapp.com/face_smile/"

        print(user_memebership.count)
        url = "http://127.0.0.1:8000/det_smile/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class Age(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_age/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class EyeStatus(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_eyestatus/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class Gender(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_gender/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class EyeGlasses(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/det_glasses/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


########################################################################################################


class BeautyScore(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/beauty_score/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class ReconstructionFace(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/face_threeD_recons/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


class OutlineBody(APIView):

    # permission_classes = (IsAuthenticated, )
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        img_path = os.path.join(settings.MEDIA_ROOT, str(post[0].image_file))

        token_id = user_memebership.customer_token_id

        # Increasing the Api count
        count = user_memebership.count
        count += 1

        user_memebership.count = count
        user_memebership.save()

        url = "http://127.0.0.1:8000/body_outline/"

        # # headers = {'Authorization': 'Token ' + str(token_id)}
        r = requests.post(url, data={"name_of_file": img_path})

        data = r.content
        dict_str = data.decode("UTF-8").replace("'", '"')

        dict_ = json.loads(dict_str)

        """
         API count wrt to user and
         its respected operation hit
        """
        # user = User.objects.get(username=user)
        #
        # title = "Skeleton"
        # Doc = APIdoc.objects.filter(title=title)
        #
        # api_user_count, flag = APIUserCount.objects.get_or_create(user=user, api_doc=Doc.first())
        # api_user_count.api_count += 1

        return Response(data=dict_, status=status.HTTP_200_OK)


#
# @api_view(
#     ["DELETE", ]
# )
# def DeleteImagesDetail(request):
#     try:
#         post = UploadImage.objects.all()
#     except UploadImage.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     if len(post) > 1:
#         if request.method == "DELETE":
#             for entry in range(len(post) - 1):
#                 img_path = os.path.join(
#                     settings.MEDIA_ROOT, str(post[entry].image_file)
#                 )
#                 os.remove(img_path)
#                 operation = post[entry].delete()
#
#         data = {}
#         if operation:
#             data[SUCCESS] = DELETE_SUCCESS
#
#         return Response(data=data)
#
#     else:
#         return Response(data={"error_msg": "no image to DELETE, Please UPLOAD to continue!!"})


class DeleteImagesDetail(APIView):

    def delete(self, request):
        try:
            post = UploadImage.objects.all()
        except UploadImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if len(post) > 1:
            if request.method == "DELETE":
                for entry in range(len(post) - 1):
                    img_path = os.path.join(
                        settings.MEDIA_ROOT, str(post[entry].image_file)
                    )
                    os.remove(img_path)
                    operation = post[entry].delete()

            data = {}
            if operation:
                data[SUCCESS] = DELETE_SUCCESS

            return Response(data=data)

        else:
            # creates SMTP session
            s = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)

            # start TLS for security
            s.starttls()

            # Authentication
            s.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
            message = f'This is a msg from Divyanshu Dimri'
            s.sendmail(settings.EMAIL_HOST_USER, "dimridd@gmail.com", message)

            # terminating the session
            s.quit()

            return Response(data={"error_msg": "no image to DELETE, Please UPLOAD to continue!!"})


class HelloView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'message': 'Hello, World!'}
        return Response(content)
