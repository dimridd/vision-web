from django.conf.urls import url
from .views import UpdateUserMembershipDetail


app_name = "memberships"

urlpatterns = [
    url(r"^update/(?P<user>\w+)/$", UpdateUserMembershipDetail.as_view(), name='update-membership_detail_')
]
