from rest_framework import serializers

from .models import UserMembership


class UpdateUserMembershipSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMembership
        fields = ("membership", "customer_token_id")

    def update(self, instance, validated_data):
        instance.membership = validated_data.get("membership", instance.membership)
        instance.save()

        return instance
