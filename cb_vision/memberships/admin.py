from django.contrib import admin

from .models import Memberships, UserMembership, Subscription
# Register your models here.

admin.site.register(Memberships)
admin.site.register(UserMembership)
admin.site.register(Subscription)
