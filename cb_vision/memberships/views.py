from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import UserMembership
from cb_vision.users.models import User

from .serializers import UpdateUserMembershipSerializer

# Create your views here.


class UpdateUserMembershipDetail(APIView):
    def get(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = UpdateUserMembershipSerializer(user_memebership)
        return Response(serializer.data)

    def put(self, request, user):
        try:
            user = User.objects.get(username=user)

        except User.DoesNotExist:
            return Response(f"User with name {user} not found in the database", status=status.HTTP_404_NOT_FOUND)

        try:
            user_memebership = UserMembership.objects.get(user=user)
        except UserMembership.DoesNotExist:
            return Response(f"User with name {user.username} not found in the database", status=status.HTTP_404_NOT_FOUND)

        serializer = UpdateUserMembershipSerializer(user_memebership, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class UpdateUserMembershipDetail(UpdateAPIView):
#     queryset = UserMembership.objects.all()
#     serializer_class = UpdateUserMembershipSerializer
#     lookup_field = "membership"
#     # permission_classes = (permissions.IsAuthenticated,)
#
#     def update(self, request, *args, **kwargs):
#         instance = self.get_object()
#         instance.name = request.data.get("membership")
#         instance.save()
#
#         serializer = self.get_serializer(instance)
#         serializer.is_valid(raise_exception=True)
#         self.perform_update(serializer)
#
#         return Response(serializer.data)
