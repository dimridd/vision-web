from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save

from model_utils.models import TimeStampedModel

from cb_vision.core.behaviours import (
    SlugMixin,
    StatusMixin,
    UserStampedMixin,
)

import uuid

# Create your models here.

MEMBERSHIP_CHOICES = (("Enterprise", "ent"), ("Professional", "pro"), ("Free", "free"))


class Memberships(SlugMixin):
    membership_type = models.CharField(
        _("Membership Choices"),
        choices=MEMBERSHIP_CHOICES,
        default="Free",
        max_length=30,
    )
    price = models.IntegerField(_("Price"), default=15)
    strip_plan_id = models.CharField(_("Plan ID"), max_length=255)

    def __str__(self):
        return self.membership_type


class UserMembership(StatusMixin, UserStampedMixin, TimeStampedModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    customer_token_id = models.CharField(_("Token ID"), max_length=255,)
    membership = models.ForeignKey(Memberships, on_delete=models.SET_NULL, null=True)
    count = models.PositiveSmallIntegerField(_("Api count"), default=0)

    def __str__(self):
        return self.user.username


def post_save_usermembership_create(sender, instance, created, *args, **kwargs):
    if created:
        UserMembership.objects.get_or_create(user=instance)

    user_membership, created = UserMembership.objects.get_or_create(user=instance)
    if (
        user_membership.customer_token_id is None
        or user_membership.customer_token_id == ""
    ):
        new_customer_id = str(uuid.uuid4()) + "_" + user_membership.user.username
        user_membership.customer_token_id = new_customer_id
        user_membership.save()


post_save.connect(post_save_usermembership_create, sender=settings.AUTH_USER_MODEL)


class Subscription(StatusMixin, UserStampedMixin, TimeStampedModel):
    user_membership = models.ForeignKey(UserMembership, on_delete=models.CASCADE)
    stripe_subscription_id = models.CharField(_("Subscription ID"), max_length=255)
    skeleton = models.IntegerField(_("Skeleton"), default=15)
    hand_gesture = models.IntegerField(_("Hand Gesture"), default=15)
    gaze_estimation = models.IntegerField(_("Gaze Estimation"), default=15)
    facial_skin_status = models.IntegerField(_("Facial Skin Status"), default=15)
    face_searching = models.IntegerField(_("Face Searching"), default=15)
    face_landmarks = models.IntegerField(_("Face Landmarks"), default=15)
    face_detection = models.IntegerField(_("Face Detection"), default=15)
    face_comparing = models.IntegerField(_("Face Comparing"), default=15)
    face_attributes_api = models.IntegerField(_("Face Attributes"), default=15)
    emotion_recognition = models.IntegerField(_("Emotion_ Recognition"), default=15)
    dense_facial_landmarks = models.IntegerField(_("Dense Facial Landmarks"), default=15)
    body_outline = models.IntegerField(_("Body Outline"), default=15)
    body_detection = models.IntegerField(_("Body Detection"), default=15)
    body_attributes = models.IntegerField(_("Body Attributes"), default=15)
    beauty_score = models.IntegerField(_("Beauty Score"), default=15)
    face_reconstruction = models.IntegerField(_("Face Reconstruction"), default=15)

    def __str__(self):
        return self.user_membership.user.username
