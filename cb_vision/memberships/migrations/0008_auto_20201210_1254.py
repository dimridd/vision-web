# Generated by Django 3.0.9 on 2020-12-10 07:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('memberships', '0007_auto_20201209_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='beauty_score',
            field=models.IntegerField(default=15, verbose_name='Beauty Score'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='body_attributes',
            field=models.IntegerField(default=15, verbose_name='Body Attributes'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='body_detection',
            field=models.IntegerField(default=15, verbose_name='Body Detection'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='body_outline',
            field=models.IntegerField(default=15, verbose_name='Body Outline'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='dense_facial_landmarks',
            field=models.IntegerField(default=15, verbose_name='Dense Facial Landmarks'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='emotion_recognition',
            field=models.IntegerField(default=15, verbose_name='Emotion_ Recognition'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='face_attributes_api',
            field=models.IntegerField(default=15, verbose_name='Face Attributes'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='face_comparing',
            field=models.IntegerField(default=15, verbose_name='Face Comparing'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='face_detection',
            field=models.IntegerField(default=15, verbose_name='Face Detection'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='face_landmarks',
            field=models.IntegerField(default=15, verbose_name='Face Landmarks'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='face_reconstruction',
            field=models.IntegerField(default=15, verbose_name='Face Reconstruction'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='face_searching',
            field=models.IntegerField(default=15, verbose_name='Face Searching'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='facial_skin_status',
            field=models.IntegerField(default=15, verbose_name='Facial Skin Status'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='gaze_estimation',
            field=models.IntegerField(default=15, verbose_name='Gaze Estimation'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='hand_gesture',
            field=models.IntegerField(default=15, verbose_name='Hand Gesture'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='skeleton',
            field=models.IntegerField(default=15, verbose_name='Skeleton'),
        ),
    ]
