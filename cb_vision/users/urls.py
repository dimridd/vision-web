from django.urls import path
from django.conf.urls import url

from cb_vision.users.views import (
    # user_detail_view,
    # user_redirect_view,
    # user_update_view,
    UserRegisterAPIVIew,
)
from .views import DeleteUserDetail, UserLoginAPIView, UserLogoutAPIView, success

app_name = "users"
urlpatterns = [
    # path("~redirect/", view=user_redirect_view, name="redirect"),
    # path("~update/", view=user_update_view, name="update"),
    # path("<str:username>/", view=user_detail_view, name="detail"),
    url(r"^register/", UserRegisterAPIVIew.as_view(), name="register"),
    url(r"^login/", UserLoginAPIView.as_view(), name="login"),
    path("delete/<str:username>/", DeleteUserDetail, name="delete-user"),
    url(r"^logout/(?P<user>\w+)/$", UserLogoutAPIView, name="logout"),
    url(r"^success/(?P<user>\w+)/$", success, name="user_success"),
    # path('login/', obtain_auth_token, name='login'),
    # re_path('^update/<str:username>/', )
]
