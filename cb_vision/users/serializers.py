from rest_framework import serializers
from rest_framework.serializers import (
    CharField,
    EmailField,
    ValidationError,
)
from rest_framework.authtoken.models import Token

from django.db.models import Q
from .models import User


class ResgistrationSerializer(serializers.ModelSerializer):
    password2 = CharField(
        label="Confirm Password", style={"input_type": "password"}, write_only=True
    )
    email = EmailField(label="Email Address")

    class Meta:
        model = User
        fields = ["email", "username", "password", "password2"]
        extra = {"password": {"write_only": True}}

    def validate(self, data):
        email = data["email"]
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError("Email Address already registered")
        return data

    def validate_password2(self, value):
        data = self.get_initial()
        password = data.get("password")
        password2 = value

        if password != password2:
            raise serializers.ValidationError({"password": "Password must match"})

    def create(self, validated_data):

        username = validated_data["username"]
        email = validated_data["email"]
        password = self.validated_data["password"]
        user_obj = User(email=email, username=username,)

        user_obj.set_password(password)
        user_obj.save()

        return validated_data


class UserLoginSerializer(serializers.ModelSerializer):
    token = CharField(allow_blank=True, read_only=True)
    username = CharField(required=False, allow_blank=True)
    email = EmailField(label="Email Address", required=False, allow_blank=True)

    class Meta:
        model = User
        fields = ["email", "username", "password", "token"]
        extra_kwargs = {"password": {"write_only": True}}

    def validate(self, data):
        user_obj = None
        email = data.get("email", None)
        username = data.get("username", None)
        password = data["password"]
        if not email and not username:
            raise ValidationError("A username or email is required")

        user = User.objects.filter(Q(email=email) and Q(username=username)).distinct()
        print(user)
        user = user.exclude(email__isnull=True).exclude(email__iexact="")

        print(user.exists())
        if user.exists() and user.count():
            user_obj = user.first()
            print(user_obj)
        else:
            raise ValidationError("This username/email is not valid")

        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError("Incorrect credentials please try again")

        data["token"] = Token.objects.get(user=user_obj).key
        return data


class DeleteUsernameSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username"]


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = "__all__"
