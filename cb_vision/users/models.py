from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from model_utils.models import TimeStampedModel


class User(AbstractUser):
    """Default user for cb_vision.
    """

    #: First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class UserLoginDetail(TimeStampedModel):

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    flag = models.IntegerField(_("Flag"), default=0)

    def __str__(self):
        return "{} | {}".format(self.user.username, self.flag)


def post_save_userlogindetail_create(sender, instance, created, *args, **kwargs):
    if created:
        UserLoginDetail.objects.get_or_create(user=instance)

    login_detail, created = UserLoginDetail.objects.get_or_create(user=instance)
    if login_detail.user is None or login_detail.user == "":
        login_detail.save()


post_save.connect(post_save_userlogindetail_create, sender=settings.AUTH_USER_MODEL)
