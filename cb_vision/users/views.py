from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.conf import settings
from django.urls import reverse
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from .serializers import ResgistrationSerializer, UserLoginSerializer
from .models import UserLoginDetail

User = get_user_model()

ERROR = "error"
DELETE_SUCCESS = "deleted"
UPDATE_SUCCESS = "updated"
SUCCESS = "success"


# class UserDetailView(LoginRequiredMixin, DetailView):
#
#     model = User
#     slug_field = "username"
#     slug_url_kwarg = "username"
#
#
# user_detail_view = UserDetailView.as_view()
#
#
# class UserUpdateView(LoginRequiredMixin, UpdateView):
#
#     model = User
#     fields = ["name"]
#
#     def get_success_url(self):
#         return reverse("users:detail", kwargs={"username": self.request.user.username})
#
#     def get_object(self):
#         return User.objects.get(username=self.request.user.username)
#
#     def form_valid(self, form):
#         messages.add_message(
#             self.request, messages.INFO, _("Infos successfully updated")
#         )
#         return super().form_valid(form)
#
#
# user_update_view = UserUpdateView.as_view()
#
#
# class UserRedirectView(LoginRequiredMixin, RedirectView):
#
#     permanent = False
#
#     def get_redirect_url(self):
#         return reverse("users:detail", kwargs={"username": self.request.user.username})
#
#
# user_redirect_view = UserRedirectView.as_view()


class UserRegisterAPIVIew(CreateAPIView):
    serializer_class = ResgistrationSerializer
    queryset = User.objects.all()


class UserLoginAPIView(APIView):
    # permission_classes = (AllowAny, )
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):

        data = request.data
        serializer = UserLoginSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            subject = 'Code Band'
            message = 'Sending Email through Gmail'
            send_mail(subject,
                      message, settings.EMAIL_HOST_USER, ["dimridd@gmail.com"], fail_silently=False)

            new_data = serializer.data
            user = User.objects.get(username=new_data["username"])
            if user:

                login_detail = UserLoginDetail.objects.get(user=user)
                login_detail.flag = 1
                login_detail.save()

                return HttpResponseRedirect(
                    reverse("users:user_success", kwargs={"user": user})
                )
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def UserLogoutAPIView(request, user):
    user = User.objects.get(username=user)
    request.user = user

    login_detail = UserLoginDetail.objects.get(user=user)

    if login_detail.flag == 1:
        login_detail.flag = 0
    else:
        raise ValueError("User is not Logged in")

    login_detail.save()
    return HttpResponseRedirect(reverse("users:login"))


@api_view(("GET",))
def success(request, user):
    data = {}

    user = User.objects.get(username=user)
    login_detail = UserLoginDetail.objects.get(user=user)
    if login_detail.flag == 1:
        token = Token.objects.get(user=user)
        data["token"] = str(token)

        return Response(data=data, status=status.HTTP_200_OK)
    else:
        raise ValueError("User is not Logged in")


@api_view(
    ["DELETE", ]
)
def DeleteUserDetail(request, username):
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "DELETE":
        operation = user.delete()
        data = {}
        if operation:
            data[SUCCESS] = DELETE_SUCCESS
        return Response(data=data)
